#!/usr/bin/fish

set tmpscript (mktemp --suffix=.py)

begin
  echo -- 'import os,sys,math'
  echo -- 'for line in sys.stdin.readlines():'
  for arg in $argv
  echo -- '  '$arg
  end
  echo -- '  pass'
  echo -- 'sys.stdout.flush()'
end > $tmpscript

python $tmpscript
rm $tmpscript
